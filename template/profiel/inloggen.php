<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<?php
require_once './template/header.php';
?>
<body>
    <div class="main-banner heading-page">
        <div class="container">
            
            <div class="row">
                <form action='' method="POST">
                    <div class="mb-3">
                        <label for="username" class="form-label">Gebruikersnaam</label>
                        <input type="text" name="username" id="username" class="form-control">
                        
                    </div>
                    <div class="mb-3">
                        <label for="password" class="form-label">Wachtwoord</label>
                        <input type="password" name="password" id="password" class="form-control" >
                    </div>
                    <input type="submit" name="submit" value='login' class="btn btn-primary">
                </form>
            </div>
            <div>
                <a href="registreren"><p> nog geen account klik dan hier voor aanmelden</p></a>
            </div>
        </div>
    </div>
    <h1></h1>
    <?php
        if (isset($_POST['submit'])){
        $username = $_POST['username'];
        $password = $_POST['password'];

        $foundUser = $Users->findOne(['username' => "$username"]);
        if(!empty($foundUser)){
            if (password_verify($password, $foundUser['password']) == true){
            $_SESSION['id'] = $foundUser['_id'];
            $_SESSION['logged_in'] = 'yes';
            die(header("Location:index"));
            }else{
                echo 'incorrect wachtwoord';
            }
        }else{
            echo 'incorrect gebruikersnaam';
        }
        
    }else{

    }
    ?>

</body>
</html>