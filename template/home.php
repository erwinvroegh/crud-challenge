<html lang="en">

<head>
    <title>CRUD Challenge</title>
    <link rel="shortcut icon" href="./assets/images/logo.png">
</head>
<?php
require_once 'header.php';
?>

<body>

    <div class="main-banner header-text">
        <div class="container-fluid">
            <div class="owl-banner owl-carousel">
                <div class="item">
                    <img src="./assets/images/Roti.jpg" alt="Roti">
                    <div class="item-content">

                        <div class="main-content">

                            <a href="recepten/recepten_full.php">
                                <h4>Roti</h4>
                            </a>

                            <ul class="post-info">
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptate, dolor qui animi ab iste.</li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="item">
                    <img src="./assets/images/Rijstsalade.jpg" alt="Rijstsalade">
                    <div class="item-content">

                        <div class="main-content">

                            <a href="recepten/recepten_full.php">
                                <h4>Rijstsalade</h4>
                            </a>

                            <ul class="post-info">
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptate, dolor qui animi ab iste.</li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="item">
                    <img src="./assets/images/PadThai.jpg" alt="Pad Thai">
                    <div class="item-content">

                        <div class="main-content">

                            <a href="recepten/recepten_full.php">
                                <h4>Pad Thai</h4>
                            </a>

                            <ul class="post-info">
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptate, dolor qui animi ab iste.</li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="item">
                    <img src="./assets/images/Enchilada.jpg" alt="Enchilada">
                    <div class="item-content">

                        <div class="main-content">

                            <a href="recepten/recepten_full.php">
                                <h4>Enchilada</h4>
                            </a>

                            <ul class="post-info">
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptate, dolor qui animi ab iste.</li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="item">
                    <img src="./assets/images/CremeBrulee.jpg" alt="Crème Brûlée">
                    <div class="item-content">

                        <div class="main-content">

                            <a href="recepten/recepten_full.php">
                                <h4>Crème Brûlée</h4>
                            </a>

                            <ul class="post-info">
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptate, dolor qui animi ab iste.</li>
                            </ul>
                        </div>

                    </div>
                </div>
                <div class="item">
                    <img src="./assets/images/TurksePizza.jpg" alt="Turkse Pizza">
                    <div class="item-content">
                        <div class="main-content">

                            <a href="recepten/recepten_full.php">
                                <h4>Turkse Pizza</h4>
                            </a>

                            <ul class="post-info">
                                <li>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Velit voluptate, dolor qui animi ab iste.</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <br><br> <br>
    <h2 class="text-center">Welkom, bij koekenpan</h2>
    <p class="text-center">Natuurlijk geniet je graag elk moment van de dag van wat lekkers.<br> Met de recepten van Koekenpan is dat gelukkig geen probleem. <br>Bekijk al onze recepten en zo vind je vast en zeker snel een recept waar jij helemaal blij van wordt!</p>
    <br>
    <hr>
    <section class="blog-posts grid-system">
        <div class="container">
            <div class="all-blog-posts">
                <h2 class="text-center">Voorgestelde Recepten</h2>
                <br>
                <div class="row">
                    <div class="col-md-4 col-sm-6">
                        <div class="blog-post">
                            <div class="blog-thumb">
                                <img src="./assets/images/Gehaktbal.jpg" alt="Gehaktballetjes">
                            </div>
                            <div class="down-content">
                                <a href="recepten/recepten_full.php">
                                    <h4>Grootmoeders Gehaktballetjes</h4>
                                </a>
                                <p>Zelf gehaktballen maken is hartstikke makkelijk en het wordt veel gedaan in Nederland. Ik denk daarom ook dat er best wel wat mensen zullen zijn die een eigen ‘familiereceptuur’ hebben hiervoor. Maar voor iedereen die dat niet heeft heb ik hier een recept om zelf zalige gehaktballen (met jus natuurlijk) te kunnen bereiden.</p>
                                <div class="post-options">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <ul class="post-tags">
                                                <li><a href="recepten/recepten_full.php">Bekijk Recept</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="blog-post">
                            <div class="blog-thumb">
                                <img src="./assets/images/Kapsalon.jpg" alt="Kapsalon">
                            </div>
                            <div class="down-content">
                                <a href="recepten/recepten_full.php">
                                    <h4>Heerlijke Kapsalon</h4>
                                </a>
                                <p>Een kapsalon is een gerecht bestaande uit friet bedekt met shoarma, afgetopt met kaas, even onder de grill gezet, zodat de kaas smelt, met bovenop salade. Vaak wordt de kapsalon geserveerd met knoflooksaus en sambal. In Suriname wordt de kapsalon geserveerd met knoflooksaus en Javaans-Surinaamse pindasaus. In plaats van shoarma, wordt de kapsalon ook wel klaargemaakt met kebab, döner, gyros, kip of falafel.</p>
                                <div class="post-options">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <ul class="post-tags">
                                                <li><a href="recepten/recepten_full.php">Bekijk Recept</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 col-sm-6">
                        <div class="blog-post">
                            <div class="blog-thumb">
                                <img src="./assets/images/Tagliatelle.jpg" alt="Tagliatelle">
                            </div>
                            <div class="down-content">
                                <a href="recepten/recepten_full.php">
                                    <h4>Tagliatelle Pesto Kip</h4>
                                </a>
                                <p>Tagliatelle, we zijn er dol op! Tagliatelle is misschien wel de meest populaire pastasoort. En dat is niet gek. Tagliatelle is makkelijk om te koken en met een simpele saus zet je in een handomdraai een overheerlijk gerecht op tafel. De brede linten lenen zich natuurlijk perfect voor tomatensaus, roomsaus en pesto. Een ding is zeker: welke saus je ook serveert bij tagliatelle, het is altijd goed.</p>
                                <div class="post-options">
                                    <div class="row">
                                        <div class="col-lg-12">
                                            <ul class="post-tags">
                                                <li><a href="recepten/recepten_full.php">Bekijk Recept</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>

    <script src="./assets/js/custom.js"></script>
    <script src="./assets/js/owl.js"></script>
    <script src="./assets/js/slick.js"></script>
    <script src="./assets/js/isotope.js"></script>
    <script src="./assets/js/accordions.js"></script>

    <script language="text/Javascript">
        cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
        function clearField(t) { //declaring the array outside of the
            if (!cleared[t.id]) { // function makes it static and global
                cleared[t.id] = 1; // you could use true and false, but that's more typing
                t.value = ''; // with more chance of typos
                t.style.color = '#fff';
            }
        }
    </script>

</body>

<?php
require_once 'footer.php';
?>

</html>