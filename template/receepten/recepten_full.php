<html lang="en">
<head>
    <title>CRUD Challenge</title>
    <link rel="shortcut icon" href="./assets/images/logo.png" >
</head>
<?php
require_once './template/header.php';
?>
<body>
    <div class="main-banner heading-page">
                <?php
                    $user_id = $_SESSION['id'];
                    $id = $_GET['id'];
                    $recept = $Recepten->findOne(['_id' => new MongoDB\BSON\ObjectId($id)]);
                    var_dump($recept);
                    echo "<div class='col-md-4 col-sm-6'>
                        <div class='blog-post'>
                            <div class='blog-thumb'>
                                <img src='./assets/images/gerechtenpf.png' alt=''>
                            </div>
                            <div class='down-content'>
                                <h4>  " . $recept['name'] ." </h4>
                                <br>
                                <h5> ingredienten</h5>
                                <p> " .$recept['ingredients']."</p>
                                <br>
                                <h5> bereidingswijzen</h5>
                                <p> " .$recept['preparing']."</p>
                                "; 
                                if(!empty($recept['userID'])){
                                    if($recept['userID'] == $user_id){
                                        echo "
                                        <div class='post-options'>
                                            <div class='row'>
                                                <div class='col-lg-12'>
                                                    <div class='btn-group' role='group' aria-label='Basic example'>
                                                        <button type='button' class='btn btn-primary'><a href='recepten_aanpassen?id=$recept[_id]'>aanpassen</a> </button>
                                                        <button type='button' class='btn btn-primary'><a href='recepten_verwijderen?id=$recept[_id]'>verwijderen</a></button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>";
                                        }
                                }
                                
                                echo"
                                
                            </div>
                        </div>
                    </div>";
                ?>
</body>
<?php
 require_once './template/footer.php';
?>
</html>