<html lang="en">
<head>
    <title>CRUD Challenge</title>
    <link rel="shortcut icon" href="./assets/images/logo.png" >
    <!-- <link rel="stylesheet" href="./assets/css/style.css" type="text/css">
        <link rel="stylesheet" href="./assets/css/owl.css" type="text/css"> -->

</head>
<?php
require_once './template/header.php';
?>

<body>
    <section class="blog-posts grid-system">
        <div class="container">
            <div class="all-blog-posts">
                <h2 class="text-center">Recepten</h2>
                <br>
                <div class="row">
                    <?php
                    $cursor = $Recepten->find([]);
                    foreach ($cursor as $document){
                        echo "<div class='col-md-4 col-sm-6'>
                        <div class='blog-post'>
                            <div class='blog-thumb'>
                                <img src='./assets/images/gerechtenpf.png' alt=''>
                            </div>
                            <div class='down-content'>
                                <a href='recepten_full?id=$document[_id]'>
                                    <h4>  " . $document['name'] ." </h4>
                                </a>
                                <div class='post-options'>
                                    <div class='row'>
                                        <div class='col-lg-12'>
                                            <ul class='post-tags'>
                                                <li><a href='recepten_full?id=$document[_id]'>Bekijk Recept</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>";
                    }
                    ?>
                </div>
            </div>
        </div>
    </section>


    <!-- Bootstrap core JavaScript -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>

    <!-- Additional Scripts -->
    <script src="./assets/js/custom.js"></script>
    <script src="./assets/js/owl.js"></script>
    <script src="./assets/js/slick.js"></script>
    <script src="./assets/js/isotope.js"></script>
    <script src="./assets/js/accordions.js"></script>

    <script language="text/Javascript">
        cleared[0] = cleared[1] = cleared[2] = 0; //set a cleared flag for each field
        function clearField(t) { //declaring the array outside of the
            if (!cleared[t.id]) { // function makes it static and global
                cleared[t.id] = 1; // you could use true and false, but that's more typing
                t.value = ''; // with more chance of typos
                t.style.color = '#fff';
            }
        }
    </script>

</body>

<?php
 require_once './template/footer.php';
?>

</html>