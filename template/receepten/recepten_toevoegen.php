<html lang="en">
<head>
    <title>CRUD Challenge</title>
    <link rel="shortcut icon" href="./assets/images/logo.png" >
</head>
<?php
require_once './template/header.php';
?>
<body>
    <div class="main-banner heading-page">
            <div class="container">
                
                <div class="row">
                    <form action='' method="POST">
                        <div class="mb-3">
                            <label for="name" class="form-label">Naam van recept</label>
                            <input type="text" name="name" id="name" class="form-control">
                            
                        </div>
                        <div class="mb-3">
                            <label for="ingredients" class="form-label">ingredienten van recept</label>
                            <textarea name="ingredients" id="ingredients" class="form-control" rows="5" cols="50" > </textarea>
                        </div>
                        <div class="mb-3">
                            <label for="preparing" class="form-label">bereiding van recept</label>
                            <textarea name="preparing" id="preparing" class="form-control" rows="5" cols="50" ></textarea>
                        </div>
                        <input type="submit" name="submit" value='toevoegen' class="btn btn-primary">
                    </form>
                </div>
            </div>
        </div>

        <?php
            if (isset($_POST['submit'])){
            $name = $_POST['name'];
            $ingredients = $_POST['ingredients'];
            $preparing = $_POST['preparing'];
            $userID = $_SESSION['id'];
            
            $insertOneResult = $Recepten->insertOne([
                'name' => $name,
                'ingredients' => $ingredients,
                'preparing' => $preparing,
                'userID' => $userID
            ]);
            echo 'Waarde is toegevoegd!';

        }else{
            echo 'Vul waarde in';
            die;
        }
        ?>
</body>
<?php
 require_once './template/footer.php';
?>
</html>