<footer>
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <ul class="social-icons">
                    <li><a href="#">Homepage</a></li>
                    <li><a href="#">Recepten</a></li>
                    <li><a href="#">Recepten Toevoegen</a></li>
                    <li><a href="#">Profiel</a></li>
                </ul>
                <ul class="social-icons">
                <li><a href="https://www.instagram.com/" target="_blank"><img border="0" alt="instagram" src="./assets/images/instagram.png" width="50" height="50"></a></li>
                <li><a href="https://www.facebook.com/" target="_blank"><img border="0" alt="instagram" src="./assets/images/facebook.png" width="50" height="50"></a></li>
                <li><a href="https://www.twitter.com/" target="_blank"><img border="0" alt="instagram" src="./assets/images/twitter.png" width="40" height="40"></a></li>
                <li><a href="https://www.linkedin.com/" target="_blank"><img border="0" alt="instagram" src="./assets/images/linkedin.png" width="40" height="40"></a></li>
            </ul>
            </div>
        </div>
    </div>
</footer>