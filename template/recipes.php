<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
</head>
<?php


require_once 'header.php';
?>
<body>
    <?php 
    $recipes = '[
        {
        "name": "Roti",
        "ingredients": "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit",
        "preparing": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi efficitur vel sapien et condimentum. Quisque ligula mauris, mattis accumsan quam et, sodales convallis dolor. Suspendisse consequat ac nisl non sodales. Ut dapibus, augue ut bibendum egestas, lorem augue tempus eros, vestibulum euismod dui libero fringilla tellus. Praesent tincidunt facilisis pellentesque. Cras euismod magna eget purus convallis, quis maximus ante condimentum. Nunc congue justo vitae neque cursus, eget eleifend quam ullamcorper. Nulla vitae tincidunt purus, non imperdiet tellus. Ut mattis neque et mi porta scelerisque. Donec non convallis neque."
        },
        {
        "name": "Rijstsalade",
        "ingredients": "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit",
        "preparing": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi efficitur vel sapien et condimentum. Quisque ligula mauris, mattis accumsan quam et, sodales convallis dolor. Suspendisse consequat ac nisl non sodales. Ut dapibus, augue ut bibendum egestas, lorem augue tempus eros, vestibulum euismod dui libero fringilla tellus. Praesent tincidunt facilisis pellentesque. Cras euismod magna eget purus convallis, quis maximus ante condimentum. Nunc congue justo vitae neque cursus, eget eleifend quam ullamcorper. Nulla vitae tincidunt purus, non imperdiet tellus. Ut mattis neque et mi porta scelerisque. Donec non convallis neque."
        },
        {
        "name": "Pad Thai",
        "ingredients": "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit",
        "preparing": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi efficitur vel sapien et condimentum. Quisque ligula mauris, mattis accumsan quam et, sodales convallis dolor. Suspendisse consequat ac nisl non sodales. Ut dapibus, augue ut bibendum egestas, lorem augue tempus eros, vestibulum euismod dui libero fringilla tellus. Praesent tincidunt facilisis pellentesque. Cras euismod magna eget purus convallis, quis maximus ante condimentum. Nunc congue justo vitae neque cursus, eget eleifend quam ullamcorper. Nulla vitae tincidunt purus, non imperdiet tellus. Ut mattis neque et mi porta scelerisque. Donec non convallis neque."
        },
        {
        "name": "Enchilada",
        "ingredients": "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit",
        "preparing": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi efficitur vel sapien et condimentum. Quisque ligula mauris, mattis accumsan quam et, sodales convallis dolor. Suspendisse consequat ac nisl non sodales. Ut dapibus, augue ut bibendum egestas, lorem augue tempus eros, vestibulum euismod dui libero fringilla tellus. Praesent tincidunt facilisis pellentesque. Cras euismod magna eget purus convallis, quis maximus ante condimentum. Nunc congue justo vitae neque cursus, eget eleifend quam ullamcorper. Nulla vitae tincidunt purus, non imperdiet tellus. Ut mattis neque et mi porta scelerisque. Donec non convallis neque."
        },
        {
        "name": "Crème Brûlée",
        "ingredients": "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit",
        "preparing": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi efficitur vel sapien et condimentum. Quisque ligula mauris, mattis accumsan quam et, sodales convallis dolor. Suspendisse consequat ac nisl non sodales. Ut dapibus, augue ut bibendum egestas, lorem augue tempus eros, vestibulum euismod dui libero fringilla tellus. Praesent tincidunt facilisis pellentesque. Cras euismod magna eget purus convallis, quis maximus ante condimentum. Nunc congue justo vitae neque cursus, eget eleifend quam ullamcorper. Nulla vitae tincidunt purus, non imperdiet tellus. Ut mattis neque et mi porta scelerisque. Donec non convallis neque."
        },
        {
        "name": "Turkse Pizza",
        "ingredients": "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit",
        "preparing": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi efficitur vel sapien et condimentum. Quisque ligula mauris, mattis accumsan quam et, sodales convallis dolor. Suspendisse consequat ac nisl non sodales. Ut dapibus, augue ut bibendum egestas, lorem augue tempus eros, vestibulum euismod dui libero fringilla tellus. Praesent tincidunt facilisis pellentesque. Cras euismod magna eget purus convallis, quis maximus ante condimentum. Nunc congue justo vitae neque cursus, eget eleifend quam ullamcorper. Nulla vitae tincidunt purus, non imperdiet tellus. Ut mattis neque et mi porta scelerisque. Donec non convallis neque."
        },
        {
        "name": "Grootmoeders Gehaktballetjes",
        "ingredients": "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit",
        "preparing": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi efficitur vel sapien et condimentum. Quisque ligula mauris, mattis accumsan quam et, sodales convallis dolor. Suspendisse consequat ac nisl non sodales. Ut dapibus, augue ut bibendum egestas, lorem augue tempus eros, vestibulum euismod dui libero fringilla tellus. Praesent tincidunt facilisis pellentesque. Cras euismod magna eget purus convallis, quis maximus ante condimentum. Nunc congue justo vitae neque cursus, eget eleifend quam ullamcorper. Nulla vitae tincidunt purus, non imperdiet tellus. Ut mattis neque et mi porta scelerisque. Donec non convallis neque."
        },
        {
        "name": "Heerlijke Kapsalon",
        "ingredients": "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit",
        "preparing": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi efficitur vel sapien et condimentum. Quisque ligula mauris, mattis accumsan quam et, sodales convallis dolor. Suspendisse consequat ac nisl non sodales. Ut dapibus, augue ut bibendum egestas, lorem augue tempus eros, vestibulum euismod dui libero fringilla tellus. Praesent tincidunt facilisis pellentesque. Cras euismod magna eget purus convallis, quis maximus ante condimentum. Nunc congue justo vitae neque cursus, eget eleifend quam ullamcorper. Nulla vitae tincidunt purus, non imperdiet tellus. Ut mattis neque et mi porta scelerisque. Donec non convallis neque."
        },
        {
        "name": "Tagliatelle Pesto Kip",
        "ingredients": "Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit",
        "preparing": "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Morbi efficitur vel sapien et condimentum. Quisque ligula mauris, mattis accumsan quam et, sodales convallis dolor. Suspendisse consequat ac nisl non sodales. Ut dapibus, augue ut bibendum egestas, lorem augue tempus eros, vestibulum euismod dui libero fringilla tellus. Praesent tincidunt facilisis pellentesque. Cras euismod magna eget purus convallis, quis maximus ante condimentum. Nunc congue justo vitae neque cursus, eget eleifend quam ullamcorper. Nulla vitae tincidunt purus, non imperdiet tellus. Ut mattis neque et mi porta scelerisque. Donec non convallis neque."
        }
    ]';
    $json = json_decode($recipes, true);
    print_r($json);
    $insertOneResult = $Recepten->insertMany($json);
    ?>
</body>
</html>