    <link rel="stylesheet" href="./assets/css/style.css" type="text/css">
    <link rel="stylesheet" href="./assets/css/owl.css" type="text/css">
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-0evHe/X+R7YkIZDRvuzKMRqM+OrBnVFBL6DOitfPri4tjfHxaWutUpFmBp4vmVor" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-pprn3073KE6tl6bjs2QrFaJGz5/SUsLqktiwsUTF55Jfv3qYSDhgCecCxMW52nD2" crossorigin="anonymous"></script>
    <header class="topheader">
        <nav class="navbar navbar-expand-lg">
            <div class="container">
                <img src="./assets/images/logo.png" alt="Logo" style="width:3%;height:3%;">
                <a class="navbar-brand" href="index">
                    <h2>&nbsp &nbsp &nbsp Koekenpan<em>.</em></h2>
                </a>
                <div class="collapse navbar-collapse" id="navbarResponsive">
                    <ul class="navbar-nav ml-auto">

                        <li class="nav-item">
                            <a class="nav-link" href="index">Home</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="recepten">Recepten</a>
                        </li>

                        
                        <?php
                        if (!empty($_SESSION['logged_in'])) {
                            echo ' 
                            <li class="nav-item">
                                <a class="nav-link" href="recepten_toevoegen">Recepten Toevoegen</a>
                            </li>
                            <li class="nav-item dropdown">
                                        <a class="nav-link dropdown-toggle" id="navbarDropdown" data-toggle="dropdown" href="#" data-bs-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Profiel</a>

                                        <div class="dropdown-menu"  aria-labelledby="navbarDropdown">
                                            <a class="dropdown-item" href="uitloggen">Uitloggen</a>
                                        </div>
                                    </li>';
                        } else {
                            echo '
                            <li class="nav-item">
                                <a class="nav-link" href="inloggen">Inloggen</a>
                            </li>';
                        }
                        ?>

                    </ul>
                </div>
            </div>
        </nav>
    </header>