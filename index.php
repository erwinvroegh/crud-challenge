<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
require 'vendor/autoload.php'; // include Composer's autoloader
$Mongo = new MongoDB\Client("mongodb://localhost:27017");
$Users = $Mongo->CRUD->Users;
$Recepten = $Mongo->CRUD->Recepten;
$request = substr($_SERVER['REQUEST_URI'],1);
// var_dump($request);
?>
<?php
$params = preg_split("/(\/|\?|=)/", $request);
// $params = explode("?", $params);
// var_dump($params);

switch($params[1]){
    case 'index':
        include_once "template/home.php";
        break;
    case 'recepten':
        include_once "template/receepten/recepten.php";
        break;
    case 'recepten_toevoegen':
        include_once "template/receepten/recepten_toevoegen.php";
        break;
    case 'recepten_full':
        include_once "template/receepten/recepten_full.php";
        break;
    case 'recepten_aanpassen':
        include_once "template/receepten/recepten_aanpassen.php";
        break;
    case 'recepten_verwijderen':
        include_once "template/receepten/recepten_verwijderen.php";
        break;
    case 'uitloggen':
        include_once "template/profiel/uitloggen.php";
        break;
    case 'inloggen':
        include_once "template/profiel/inloggen.php";
        break;
    case 'registreren':
        include_once "template/profiel/registreren.php";
        break;
    default:
    include_once "template/home.php";
}
?>
